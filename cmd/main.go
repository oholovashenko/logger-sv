package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/oholovashenko/logger-sv/internal/grpc"
	"bitbucket.org/oholovashenko/logger-sv/internal/output"
	"bitbucket.org/oholovashenko/logger-sv/internal/processor"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func init() {
	pflag.String("generator_sv_dsn", "localhost:55555", "Generator service URL")
	pflag.Int("connection_retries", 10, "Number of reconnection attempts")
	pflag.Duration("connection_retries_period", 5*time.Second, "Period of reconnection attempts")

	pflag.String("file_path", "./testdata/output.txt", "File path for result log")
	pflag.Int("flow_speed", 10*1024*1024, "Flow speed limit, B/s")
	pflag.Int("buffer_size", 50*1024*1024, "Size of an output buffer, B")
	pflag.Uint("log_key", 1234, "Size of an output buffer, 4 digit unsigned integer")
	pflag.Bool("backpressure", true, "Apply backpressure along system. If false - system loses data")
	pflag.Bool("encrypt", true, "Apply encryption. Default true")

	pflag.Parse()
	_ = viper.BindPFlags(pflag.CommandLine)
	viper.AutomaticEnv()
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	errC := make(chan error, 1)

	logger := log.New(os.Stdout, "[main] ", 0)

	c := grpc.NewClient(viper.GetString("generator_sv_dsn"))
	retriableClient := grpc.NewRetriableClient(
		c,
		viper.GetInt("connection_retries"),
		viper.GetDuration("connection_retries_period"),
	)

	logWriter, err := output.NewLogWriter(
		viper.GetString("file_path"),
		viper.GetInt("flow_speed"),
		viper.GetInt("buffer_size"),
	)
	if err != nil {
		logger.Fatal(err.Error())
	}
	defer func() {
		if err := logWriter.Close(); err != nil {
			logger.Println(fmt.Errorf("failed to close log file: %w", err))
		}
	}()
	go func() { errC <- logWriter.Run(ctx) }()

	processor := processor.NewEntryProcessor(
		retriableClient,
		logWriter,
		viper.GetBool("encrypt"),
		viper.GetUint("log_key"),
		viper.GetBool("backpressure"),
	)
	go func() { errC <- processor.Run(ctx) }()

	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, os.Interrupt)
	select {
	case <-sigC:
		logger.Println("received termination signal")
	case err := <-errC:
		logger.Printf("fatal error occured %s", err.Error())
	}
}
