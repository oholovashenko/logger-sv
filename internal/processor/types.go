package processor

import "context"

type EntryProducer interface {
	Consume(context.Context, chan<- Entry) error
}

type EntryMediator interface {
	Process(Entry) error
}
