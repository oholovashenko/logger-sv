package processor

import (
	"context"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type EntryProcessor struct {
	producer EntryProducer
	output   io.Writer

	key []byte

	doEncode     bool
	backPressure bool

	logger *log.Logger
}

func NewEntryProcessor(producer EntryProducer, writer io.Writer, doEncode bool, key uint, backPressure bool) *EntryProcessor {
	return &EntryProcessor{
		producer:     producer,
		output:       writer,
		key:          []byte(strings.Repeat(strconv.FormatUint(uint64(key), 10), 8)),
		doEncode:     doEncode,
		backPressure: backPressure,
		logger:       log.New(os.Stdout, "[processor] ", 0),
	}
}

func (ep EntryProcessor) Run(ctx context.Context) error {
	upstream := make(chan Entry, 1)

	errC := make(chan error, 1)
	go func() {
		errC <- ep.producer.Consume(ctx, upstream)
	}()

	ic := make(chan Entry, 1)
	go func() {
		ep.runConveyor(ctx, ic)
	}()

	var lost uint64
	logTim := time.NewTicker(time.Second)

	for {
		select {
		case e := <-upstream:
			if ep.backPressure {
				select {
				case ic <- e:
				case <-ctx.Done():
				}
			} else {
				select {
				case ic <- e:
				default:
					lost++
				}
			}
		case err := <-errC:
			return err
		case <-ctx.Done():
			return ctx.Err()
		case <-logTim.C:
			if lost > 0 {
				ep.logger.Printf("losing messages! lost %d messages\n", lost)
				lost = 0
			}
		}
	}
}

func (np EntryProcessor) runConveyor(ctx context.Context, ic chan Entry) {
	// A good place to make a buffer at the stage of processing:
	// 		for example, a buffer in a form of a linked list of size N to preserve order
	//		and goroutine pool of same size to parallelize encryption and stuff

	for {
		select {
		case e := <-ic:
			_ = np.Process(e)
		case <-ctx.Done():
			return
		}
	}
}

func (np EntryProcessor) Process(e Entry) error {
	if np.doEncode {
		var err error
		if e, err = e.Encode(np.key); err != nil {
			np.logger.Printf("error while encoding entry %s:  %s\n", e, err.Error())
			return err
		}
	}

	var n int
	p := []byte(e.String())

	for n < len(e.String()) {
		nw, err := np.output.Write(p[n:])
		if err == io.ErrShortWrite {
			// stub. unused branch in a case of blocking Write
			time.Sleep(time.Millisecond * 10)
		} else {
			if err != nil {
				np.logger.Printf("error while writing entry:  %s\n", err.Error())
				return err
			}
		}

		n += nw
	}

	return nil
}
