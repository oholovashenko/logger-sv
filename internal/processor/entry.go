package processor

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
)

type Entry struct {
	SequenceNum uint64
	Payload     string

	encrypted  bool
	ciphertext []byte
}

func (e Entry) Encode(key []byte) (Entry, error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return Entry{}, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return Entry{}, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return Entry{}, err
	}

	ciphertext := gcm.Seal(nonce, nonce, []byte(e.Payload), nil)
	return Entry{
		SequenceNum: e.SequenceNum,
		Payload:     e.Payload,
		encrypted:   true,
		ciphertext:  ciphertext,
	}, nil
}

func (e Entry) String() string {
	if !e.encrypted {
		return fmt.Sprintf("%d:\n %s\n\n\n", e.SequenceNum, e.Payload)
	}

	return fmt.Sprintf("%d:\n %s\n\n\n", e.SequenceNum, base64.StdEncoding.EncodeToString(e.ciphertext))
}
