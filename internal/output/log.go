package output

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	"golang.org/x/sync/errgroup"
)

// TODO: make a blocking Write call wrapper

type LogWriter struct {
	*LimitedConcurrentWriter

	file      *os.File
	flushRate time.Duration
}

func NewLogWriter(path string, flowSpeed, bufferSize int) (*LogWriter, error) {
	file, err := os.OpenFile(modifyFilename(path), os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0777)
	if err != nil {
		return nil, err
	}

	return &LogWriter{
		LimitedConcurrentWriter: NewLimitedConcurrentWriterSize(file, flowSpeed, bufferSize),
		file:                    file,
		flushRate:               calculateFlushRate(flowSpeed, bufferSize),
	}, nil
}

func (lw *LogWriter) Close() error {
	var err error

	// depending on requirements, fsync may be performed here

	if err = lw.file.Close(); err != nil {
		// repeate a call as in BoltDB approach
		// a place for improvement
		err = lw.file.Close()
	}

	return err
}

func (lw *LogWriter) Run(ctx context.Context) error {
	g := new(errgroup.Group)
	g.Go(func() error {
		ticker := time.NewTicker(lw.flushRate)

		for {
			select {
			case <-ticker.C:
				if err := lw.Flush(); err != nil {
					return err
				}
			case <-ctx.Done():
				return ctx.Err()
			}
		}
	})

	return g.Wait()
}

func calculateFlushRate(flowSpeed, bufSize int) time.Duration {
	if flowSpeed <= bufSize {
		return time.Second
	}

	return time.Duration(int(time.Second) / (flowSpeed / bufSize))
}

// to easy debugging and demonstration append current time to provided filename respecting nested path format
// in production such format should be specificated before making assumptions
func modifyFilename(path string) string {
	chunks := strings.Split(path, ".")
	if len(chunks) > 0 {
		chunks[len(chunks)-2] = fmt.Sprintf("%s_%s", chunks[len(chunks)-2], time.Now().UTC().Format(time.RFC3339))
		return strings.Join(chunks, ".")
	}
	return path
}
