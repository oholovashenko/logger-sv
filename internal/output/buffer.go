package output

import (
	"io"
	"sync"
)

type LimitedConcurrentWriter struct {
	wr io.Writer

	buf   []byte
	n     int
	limit int

	mu sync.Mutex
}

func NewLimitedConcurrentWriterSize(w io.Writer, flushLimit, bufferSize int) *LimitedConcurrentWriter {
	return &LimitedConcurrentWriter{
		wr:    w,
		buf:   make([]byte, bufferSize),
		limit: min(flushLimit, bufferSize),
	}
}

func (b *LimitedConcurrentWriter) Write(p []byte) (int, error) {
	b.mu.Lock()
	defer b.mu.Unlock()

	for len(p) > (len(b.buf) - b.n) {
		n := copy(b.buf[b.n:], p)
		b.n += n
		return n, io.ErrShortWrite
	}

	n := copy(b.buf[b.n:], p)
	b.n += n
	return n, nil
}

func (b *LimitedConcurrentWriter) Flush() error {
	b.mu.Lock()
	defer b.mu.Unlock()

	if b.n == 0 {
		return nil
	}

	nn := min(b.limit, b.n)

	n, err := b.wr.Write(b.buf[0:nn])
	if n < nn && err == nil {
		err = io.ErrShortWrite
	}

	if err != nil {
		if n > 0 && n < b.n {
			copy(b.buf[0:b.n-n], b.buf[n:b.n])
		}
		b.n -= n
		return err
	}

	copy(b.buf[0:b.n-nn], b.buf[nn:b.n])
	b.n -= nn

	return nil
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}
