package grpc

import "errors"

var (
	ErrConnectionEstablishmentFailed = errors.New("Failed establishing connection")
	ErrGRPCServiceClosed             = errors.New("GRPC remote server unavailable")
)
