package grpc

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"bitbucket.org/oholovashenko/generator-sv/pkg/protos"
	"bitbucket.org/oholovashenko/logger-sv/internal/processor"
)

type Client struct {
	serverDSN string

	logger *log.Logger
}

func NewClient(dsn string) *Client {
	return &Client{
		serverDSN: dsn,
		logger:    log.New(os.Stdout, "[grpc] ", 0),
	}
}

func (c Client) dial() (*grpc.ClientConn, error) {
	// transport level security disabled
	// assumed this component interact over internal infrasctructure only
	conn, err := grpc.Dial(c.serverDSN, grpc.WithInsecure())
	if err != nil {
		c.logger.Println(fmt.Errorf("failed to dial grpc service %s: %w", c.serverDSN, err))
		return nil, err
	}

	return conn, err
}

func (c Client) Consume(ctx context.Context, downstream chan<- processor.Entry) error {
	conn, err := c.dial()
	if err != nil {
		return ErrConnectionEstablishmentFailed
	}
	defer conn.Close()

	genClient := protos.NewGeneratorServiceClient(conn)

	req := &protos.SubscribeRequest{}
	stream, err := genClient.NumbersTopic(ctx, req)
	if err != nil {
		c.logger.Println(fmt.Errorf("failed to subscribe topic: %w", err))
		return ErrConnectionEstablishmentFailed
	}

	receivedNum := 0
	loggerTimer := time.NewTicker(time.Second)
	defer loggerTimer.Stop()

	for {
		var m *protos.Number
		m, err = stream.Recv()
		if err != nil {
			st, ok := status.FromError(err)
			if !ok {
				return err
			}

			if st.Code() == codes.Unavailable {
				return ErrGRPCServiceClosed
			}

			c.logger.Println(fmt.Errorf("error while receiving from server: %w", err))
			return err
		}

		e := processor.Entry{Payload: m.GetNum(), SequenceNum: m.GetSequenceNumber()}

		select {
		case downstream <- e:
			receivedNum++
		case <-ctx.Done():
			return stream.CloseSend()
		}

		select {
		default:
		case <-loggerTimer.C:
			c.logger.Printf("received %d numbers \n", receivedNum)
			receivedNum = 0
		}
	}
}
