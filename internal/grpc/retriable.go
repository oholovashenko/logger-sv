package grpc

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/oholovashenko/logger-sv/internal/processor"
)

type RetriableClient struct {
	processor.EntryProducer

	retryLimit int
	period     time.Duration

	logger *log.Logger
}

func NewRetriableClient(c processor.EntryProducer, retries int, period time.Duration) *RetriableClient {
	return &RetriableClient{
		EntryProducer: c,
		retryLimit:    retries,
		period:        period,
		logger:        log.New(os.Stdout, "[grpc] ", 0),
	}
}

func (c RetriableClient) Consume(ctx context.Context, d chan<- processor.Entry) error {
	var retries int
	var err error

	for retries < c.retryLimit {
		if err = c.EntryProducer.Consume(ctx, d); err != nil {
			if errors.Is(err, ErrConnectionEstablishmentFailed) {
				retries++
				c.logger.Printf("GRPC connecting failed. Retrying connection in %s", c.period)
			} else if errors.Is(err, ErrGRPCServiceClosed) {
				retries = 0
				c.logger.Printf("GRPC service gone. Retrying connection in %s", c.period)
			} else {
				err = fmt.Errorf("Unknown GPRC client error %w", err)
				c.logger.Printf("GRPC failed with %v", err)
				return err
			}
		}

		tim := time.NewTimer(c.period)
		select {
		case <-tim.C:
		case <-ctx.Done():
			return nil
		}
	}

	return err
}
