FROM golang:alpine
RUN apk update && apk add --no-cache git
WORKDIR /go/src/assignment/logger
COPY . .
RUN go mod download
RUN go mod verify
RUN CGO_ENABLED=0 GO111MODULE=auto GOARCH=amd64 GOOS=linux GOPROXY=direct GOSUMDB=off go build -o /go/bin/logger cmd/main.go
ENTRYPOINT /go/bin/logger
