module bitbucket.org/oholovashenko/logger-sv

go 1.14

require (
	bitbucket.org/oholovashenko/generator-sv v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.0
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	google.golang.org/grpc v1.27.0
)
